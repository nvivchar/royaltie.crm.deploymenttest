const process = require('process');

const stdin = process.openStdin();

let data = '';

const fieldsToPick = [
  'ephemeralStorage',
  'inferenceAccelerators',
  'proxyConfiguration',
  'ipcMode',
  'pidMode',
  'tags',
  'memory',
  'cpu',
  'requiresCompatibilities',
  'placementConstraints',
  'volumes',
  'containerDefinitions',
  'networkMode',
  'executionRoleArn',
  'taskRoleArn',
  'family',
];

stdin.on('data', (chunk) => {
  data += chunk;
});

stdin.on('end', () => {
  const taskDefinitionDescription = JSON.parse(data);
  const { taskDefinition } = taskDefinitionDescription;

  const containerDefinition = taskDefinition.containerDefinitions[0];
  const imageUrl = containerDefinition.image.split(':')[0];

  const newImageTagArgumentRegex = /--newImageTag=(?<tag>\S+)/;

  const newImageTagArgument = process.argv.find((element) =>
    newImageTagArgumentRegex.test(element),
  );

  if (!newImageTagArgument) {
    throw new Error("Missing required argument 'newImageTag'");
  }

  const newImageTag = newImageTagArgument.match(newImageTagArgumentRegex).groups
    .tag;

  const newImage = `${imageUrl}:${newImageTag}`;

  const newTaskDefinitionDescription = fieldsToPick.reduce((accum, field) => {
    if (field in taskDefinition) {
      // eslint-disable-next-line no-param-reassign
      accum[field] = taskDefinition[field];
    }

    return accum;
  }, {});

  newTaskDefinitionDescription.containerDefinitions = [
    { ...containerDefinition, image: newImage },
  ];

  const newTaskDefinitionDescriptionString = JSON.stringify(
    newTaskDefinitionDescription,
  );

  console.log(newTaskDefinitionDescriptionString);
});
