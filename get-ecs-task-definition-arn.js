const process = require('process');

const stdin = process.openStdin();

let data = '';

stdin.on('data', (chunk) => {
  data += chunk;
});

stdin.on('end', () => {
  const { taskDefinition } = JSON.parse(data);

  console.log(taskDefinition.taskDefinitionArn);
});
