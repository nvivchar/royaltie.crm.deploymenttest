import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const externalRootPath = '/api/test';
  app.setGlobalPrefix(externalRootPath);

  console.log(process.env, null, 10);

  await app.listen(3000);
}
bootstrap();

