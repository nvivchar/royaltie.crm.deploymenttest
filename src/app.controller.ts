import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import * as AWS from 'aws-sdk';

@Controller('')
export class AppController {
  private version = 1;

  constructor(private readonly appService: AppService) {
    this.init();
  }

  @Get()
  getUsers(): any[] {
    return this.appService.getUsers();
  }

  @Get('info')
  getInfo(): string {
    console.log(123);
    return `<h1>Process: ${process.pid} Version: ${this.version}</h1>`;
  }

  private init() {
    const isExecutedInECS = process.env.AWS_EXECUTION_ENV === 'AWS_ECS_FARGATE';

    if (isExecutedInECS) {
      console.log('Deployed in ECS');
    }

    AWS.config.credentials = new AWS.ECSCredentials({
      httpOptions: { timeout: 5000 }, // 5 second timeout
      maxRetries: 10, // retry 10 times
    });
    const client = new AWS.SNS({ apiVersion: '2010-03-31' });

    client.listTopics({}, function (err, data) {
      if (err) console.log(err, err.stack);
      // an error occurred
      else console.log(data); // successful response
    });
  }
}
